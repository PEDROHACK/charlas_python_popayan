# Python popayan - charlas

repositorio de charlas de python popayan

## quienes somos

somos un grupo de estudio conformado por voluntarios que buscan compartir sus
experiencias en el campo de la programación y buenas prácticas de software con
otros programadores, personas que se quieran iniciar y con la comunidad en general.

nuestro objetivo es crear un espacio en el que puedan visualizarse personas,
tecnologías y buenas prácticas de la industria del software con el objetivo de
favorecer a todo el ecosistema de la industria.

para ello, usamos Python, un lenguaje de programación de alto nivel, caracterizado
por ser amigable con los usuarios, muy flexible para su uso, con una comunidad
y recursos educativos enormes y un soporte fuerte, para realizar talleres y
dar acompañamiento en los procesos.

## quienes nos apoyan

El grupo Linux Unicauca nos apoya como uno de sus grupos de estudio, permitiendonos
realizar nuestras reuniones y talleres martes y jueves de 6:15 a 7:00.

La comunidad Python Colombia nos ha reconocido como un [grupo oficial afiliado a
ellos](https://github.com/ColombiaPython/communities)

## charlas en este repositorio

- [web scraping](web_scrapping.py)

## contactanos

siguenos en nuestra pagina de [facebook](https://www.facebook.com/pythonistapopayan/)
