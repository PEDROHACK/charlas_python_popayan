"""
web scrapping es bajar y procesar contenidos de paginas en internet

requerimientos para este taller:
- beautifulsoup4
- requests
"""
import requests
from bs4 import BeautifulSoup
from datetime import datetime


class sales_scraper(object):
    'get information of products sales in popayan'

    def __init__(self):
        'initialize scrapper'
        self.url_roots = {
            'exito': 'http://www.tiendeo.com.co/popayan/exito',
            'campanario': 'https://campanariopopayan.com/ofertas'
        }

    def get_page(self, store):
        'get and parse info from store'
        # get webpage
        # parse webpage
        return page

    def get_campanario_info(self):
        'get products info from campanario'
        # find product name (h3, class: oferta-nombre , h4, oferta-tienda)
        # find product url (div, class:oferta-compartir-facebook)
        # find product description (div, class:oferta-detalle)
        # pack all in dictionary
        return products

    def get_exito_info(self):
        'get products info from exito'
        # find product name
        # find product url
        # find product description
        # pack all in dictionary
        return products


def main():
    # crear el objeto scrapper
    # crear un diccionario con las ofertas indentificadas por el almacen
    # al que pertenecen
    # mostrar al usuario las ofertas
    pass


if __name__ == '__main__':
    main()
